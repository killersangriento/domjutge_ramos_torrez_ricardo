package _Domjutge;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

class Test_trivago {

	@Test
	void test() {
		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(500, 490, 470, 400));
		assertEquals(3, trivago.respuesta(list));
		ArrayList<Integer> list2 = new ArrayList<Integer>(Arrays.asList(300 ,370, 250, 290, 279));
		assertEquals(2, trivago.respuesta(list2));
		ArrayList<Integer> list3 = new ArrayList<Integer>(Arrays.asList(1500, 1499, 1470, 1450 ,1475 ,1000));
		assertEquals(5, trivago.respuesta(list3));
	}
	@Test
	void test2() {
		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(500,468, 470, 480, 5));
		assertEquals(4, trivago.respuesta(list));
		ArrayList<Integer> list2 = new ArrayList<Integer>(Arrays.asList(400, 450, 800, 700, 1000, 480, 456, 255, 380 ,40));
		assertEquals(9, trivago.respuesta(list2));
	}
	

}
